package main.java.ops;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import main.java.ops.ocr.enums.DB;
import main.java.ops.ocr.enums.Table;

/**
 * @author avi
 *
 */
public class ConnectionManager {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String LOCAL_URL = "jdbc:mysql://localhost/ocr_ic_target2d?autoReconnect=true&useSSL=false";
	static final String LOCAL_USER = "root";
	static final String LOCAL_PASS = "5565";
	static final String P_RISDB_URL = "jdbc:mysql://ocrds.caevgc8tzcpz.us-east-1.rds.amazonaws.com/risdb?autoReconnect=true&useSSL=false";	
	static final String P_RISDB_USER = "ocuser";
	static final String P_RISDB_PASS = "ocpass!1";
	static final String P_REPORTDB_URL = "jdbc:mysql://ocrds.caevgc8tzcpz.us-east-1.rds.amazonaws.com/reportdb?autoReconnect=true&useSSL=false";	
	static final String P_REPORTDB_USER = "ocuser";
	static final String P_REPORTDB_PASS = "ocpass!1";	
	static final String Q_OC_URL = "jdbc:mysql://zollousrds.caevgc8tzcpz.us-east-1.rds.amazonaws.com/risdb?autoReconnect=true&useSSL=false";	
	static final String Q_OC_USER = "zolloususer";
	static final String Q_OC_PASS = "zollouspass!1";
	static final String schemaQuery = "select `{table}`.* from risdb.`{table}` limit 1";
	static final String unionQueryInto = "insert into `{into}` ";
	static final String unionQueryFrom = "select `{table}`.* from `{table}`";
	//	static final String newLine=System.getProperty("line.separator");
	private Connection qadbconn = null;
	private Connection risdbconn = null;
	private Connection localdbconn = null;
	private Connection reportdbconn = null;
	public final String namingTimestamp;
	private HashMap<String,String> results ;
	private HashMap<String,String> namingMap ;
	private HashMap<String,String> columnNames;
	public final String sqlTimestamp;
	public ConnectionManager() {
		super();
		DateFormat namingDf = new SimpleDateFormat("_yyyy_MM_dd_HH_mm_ss");
		DateFormat sqlDf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		this.namingTimestamp = namingDf.format(date);
		this.sqlTimestamp = sqlDf.format(date);
		
		this.results = new HashMap<String,String>();
		
		this.namingMap = new HashMap<String,String>();
		this.namingMap.put("raw_item", "rri");
		this.namingMap.put("raw_receipt", "rr");
		this.namingMap.put("snapshot", "s");
		this.namingMap.put("receipt_item", "ri");
		this.namingMap.put("receipt", "r");
		this.columnNames = new HashMap<String,String>();
		this.columnNames.put("s_name", "store_name");
		createUpdate("set sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'");
	}
	private Connection getConnection(DB db){
		String url = null;
		String user = null;
		String pass = null;
		Connection conn = null;
		switch(db)
		{
		case P_RIS:
			conn=risdbconn;
			url = P_RISDB_URL;
			user = P_RISDB_USER;
			pass = P_RISDB_PASS;
			break;
		case LOCAL:
			conn=localdbconn;
			url = LOCAL_URL;
			user = LOCAL_USER;
			pass = LOCAL_PASS;
			break;
		case Q_RIS:
			conn=qadbconn;
			url = Q_OC_URL;
			user = Q_OC_USER;
			pass = Q_OC_PASS;
			break;
		case P_REPORTDB:
			conn=reportdbconn;
			url = P_REPORTDB_URL;
			user = P_REPORTDB_USER;
			pass = P_REPORTDB_PASS;
			break;
		default:
			break;
		}
		if(conn == null){
			try {
				Class.forName(JDBC_DRIVER);
				System.out.println("Connecting to database..."+db.toString());
				conn = DriverManager.getConnection(url, user, pass);
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		switch(db)
		{
		case P_RIS:
			risdbconn=conn;
			break;
		case LOCAL:
			localdbconn=conn;
			break;
		case Q_RIS:
			qadbconn=conn;
			break;
		case P_REPORTDB:
			reportdbconn=conn;
		default:
			break;
		}
		return conn;
	}
	public void close(){
		try {
			if(risdbconn != null) 	risdbconn.close();
			if(localdbconn != null) localdbconn.close();
			if(qadbconn != null)	qadbconn.close();
			if(reportdbconn != null)	reportdbconn.close();
			System.out.println("DB Connections closed.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void copy(String query, DB from, DB to, String tableName,String retailerName) throws SQLException {
		try (PreparedStatement s1 = getConnection(from).prepareStatement(query.replace("{table}", tableName));
				ResultSet rs = s1.executeQuery())
		{
			copy(rs, from,to, tableName,retailerName);
		}
	}
	public ResultSet getSelect(String select,String from,String where,String group,String order,DB db){
		try {
			return getConnection(db).createStatement().executeQuery(
					"select "+ select+
					" from "+from+ 
					(where!=null?" where "+where:"")+
					(group!=null?" group by "+group:"")+
					(order!=null?" order by "+order:""));
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	//	public ResultSet getProductAlgDistributionResultSet(){
	//		try {
	//			return getConnection(DB.LOCAL).createStatement().executeQuery("select qri.product_alg,count(distinct qri.id) item_count from qri group by product_alg");
	//		} catch (SQLException e) {
	//			e.printStackTrace();
	//			return null;
	//		}
	//		
	//	}

	public void copy(ResultSet rs, DB from,DB to, String tableName,String retailerName) throws SQLException {
		tableName += from.name()+"_"+retailerName.replaceAll(" ","_")+namingTimestamp;
		createTableFromResultSet(rs,to,tableName); 

		int rowCounter = persistResultSet(rs, to, tableName);
		this.results.put(tableName,String.valueOf(rowCounter));

	}
	private int persistResultSet(ResultSet rs, DB to, String tableName) throws SQLException {
		ResultSetMetaData meta = rs.getMetaData();
		int rowCounter =0;
		List<String> columns = new ArrayList<>();
		for (int i = 1; i <= meta.getColumnCount(); i++)
			columns.add(meta.getColumnName(i));
		normalizeColumnNames(columns);
		try (PreparedStatement s2 = getConnection(to).prepareStatement(
				"INSERT INTO `" + tableName + "` (`"
						+ columns.stream().collect(Collectors.joining("`, `"))
						+ "`) VALUES ("
						+ columns.stream().map(c -> "?").collect(Collectors.joining(", "))
						+ ")"
				)) {

			while (rs.next()) {
				rowCounter++;
				for (int i = 1; i <= meta.getColumnCount(); i++)
					s2.setObject(i, rs.getObject(i));

				s2.addBatch();
			}

			s2.executeBatch();
		}
		return rowCounter;
	}
	private void normalizeColumnNames(List<String> columns) {
		for(String name:columnNames.keySet()){
			if (columns.contains(name)){
				columns.set(columns.indexOf(name), columnNames.get(name));
			}
		}
		
	}
	public HashMap<String, String> getResults() {
		return results;
	}
	//	private String getTimestamp() {
	//		return this.timestamp;
	//
	//	}
	public void createTableFromResultSet(ResultSet rs,DB db,String tableName) throws SQLException{

		ResultSetMetaData rsmd = rs.getMetaData();
		int columnCount = rsmd.getColumnCount();
		StringBuilder sb = new StringBuilder( 1024 );
		if ( columnCount > 0 ) { 
			sb.append( "Create table `" ).append( tableName ).append( "` ( `" );
		}
		for ( int i = 1; i <= columnCount; i ++ ) {
			if ( i > 1 ) sb.append( ", `" );
			String columnName = rsmd.getColumnLabel( i );
			String columnType = rsmd.getColumnTypeName( i );

			sb.append( columnName ).append( "` " ).append( columnType );
			if (!columnName.equals("created_on")&&!columnName.equals("obj_created_on")&&!columnName.equals("can_return_by")){
				int precision = rsmd.getPrecision( i );
				if ( precision != 0 ) {
					sb.append( "( " ).append( precision ).append( " )" );
				}
			}else if (columnName.equals("created_on")){
				sb.append(" default CURRENT_TIMESTAMP");
		}else if (columnName.equals("obj_created_on"))
			sb.append(" NULL default NULL");
		} // for columns
		sb.append( " ) " );
		getConnection(db).createStatement().executeUpdate(sb.toString()); 
	}
	public List<String> getRefs(String selectSet, DB db) throws SQLException {
		ResultSet rs = getConnection(db)
				.prepareStatement(selectSet)
				.executeQuery();
		List<String> refs = new ArrayList<String>();
		while (rs.next()) {
			refs.add(rs.getString("ref_id"));
		}
		return refs;
	}
	public void deleteByRefs(String deleteQuery,DB db, String from) throws SQLException {
		String query = deleteQuery.replace("{from}", from);
		int i = getConnection(db).createStatement().executeUpdate(query);
		System.out.println(i + " deleted from "+from);

	}
	public List<String> createScriptFromRefs(String query,DB db) throws SQLException {
		List<String> lines = new ArrayList<String>();
		ResultSet rs = getConnection(db).createStatement().executeQuery(query);
		lines.add("#! /bin/bash");

		while (rs.next()){
			lines.add(rs.getString(1));
		}
		lines.add("exit;");
		return lines;
	}
	public void consolidateResults(DB sourceDb , Table[] tables) throws SQLException{

		createTables(sourceDb,tables);

		for(Table table:tables){
			String tableString = table.toString();
			String insert = unionQueryInto.replace("{into}",tableString);
			StringJoiner unionSelect = new StringJoiner(" union ");
			for(String tableName:results.keySet()){
				if (tableName.startsWith(tableString+sourceDb)){ //added sourceDb to avoid accidental matching of receipt_item with receipt.
					unionSelect.add(unionQueryFrom.replace("{table}", tableName));
				}
			}
			String q = insert+unionSelect.toString();
			getConnection(DB.LOCAL).createStatement()
			.executeUpdate(q);

			getConnection(DB.LOCAL).createStatement().executeUpdate("RENAME TABLE "+tableString+" TO "+sourceDb.toString().charAt(0)+namingMap.get(tableString));
		}



	}
	public void createTables(DB db,Table[] tables) {
		for(Table table:tables){
			try (PreparedStatement s1 = getConnection(db).prepareStatement(schemaQuery.replace("{table}", table.toString()));
					ResultSet rs = s1.executeQuery())
			{
				createTableFromResultSet(rs,DB.LOCAL,table.toString());
			}catch (SQLException e){
				e.printStackTrace();
			}
		}

	}
	public void persistComparison(List<String> values,String qryPlq) throws SQLException {
		getConnection(DB.LOCAL).createStatement().executeUpdate("CREATE TEMPORARY TABLE `comparison_results` (\n" + 
				"  `source_ri_id` int(11) unsigned NOT NULL AUTO_INCREMENT,\n" + 
				"  `target_ri_id` int(11) unsigned NOT NULL,\n" + 
				"  `match_type` varchar(50) DEFAULT NULL,\n" + 
				"  PRIMARY KEY (`source_ri_id`)\n" + 
				") ENGINE=InnoDB DEFAULT CHARSET=latin1;");
		if (values.size()>0){
			String qry="INSERT INTO comparison_results VALUES "
					+ values.stream().collect(Collectors.joining(", "));
			getConnection(DB.LOCAL).createStatement().executeUpdate(qry);
		}
		getConnection(DB.LOCAL).createStatement().executeUpdate(qryPlq);



	}
	public void persistGarbage(List<String> garbage,String qryPrq) throws SQLException{
		getConnection(DB.LOCAL).createStatement().executeUpdate("CREATE TEMPORARY TABLE `comparison_garbage` (\n" + 
				"  `ri_id` int(11) unsigned NOT NULL,\n" + 
				"  PRIMARY KEY (`ri_id`)\n" + 
				") ENGINE=InnoDB DEFAULT CHARSET=latin1;");
		if (garbage.size()>0){
			String qry="INSERT INTO comparison_garbage VALUES "
					+ garbage.stream().collect(Collectors.joining(", "));
			getConnection(DB.LOCAL).createStatement().executeUpdate(qry);
		}
		getConnection(DB.LOCAL).createStatement().executeUpdate(qryPrq);

	}
	public void createUpdate(String qryPleftQ) {
		try {
			getConnection(DB.LOCAL).createStatement().executeUpdate(qryPleftQ);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	public void printResultSet(ResultSet rs){
		ResultSetMetaData rsmd;
		try {
			rs.beforeFirst();
			rsmd = rs.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			for (int i=1;i<=columnsNumber;i++){
				if (i>1) System.out.print("\t");
				System.out.print(rsmd.getColumnName(i));
			}
			System.out.println("");
			while (rs.next()) {
				for (int i = 1; i <= columnsNumber; i++) {
					if (i > 1) System.out.print("\t");
					System.out.print(rs.getString(i));
				}
				System.out.println("");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	public void persistMeasurement(ResultSet rs, DB db,String table) {
		try {
			rs.beforeFirst();
			persistResultSet(rs,db,table);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void createTable(String qry) throws SQLException{
		getConnection(DB.LOCAL).createStatement().executeUpdate(qry);
	}
}
