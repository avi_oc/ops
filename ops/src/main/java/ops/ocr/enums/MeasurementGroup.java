package main.java.ops.ocr.enums;

public enum MeasurementGroup {
	A,B,C,D,
	truth_C,truth_D,
	T,TW,TWM,TWMW,TWW,
	Target,Walmart,Meijer,Walgreens
}
