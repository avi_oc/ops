package main.java.ops.ocr;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.stream.Collectors;

import main.java.ops.ConnectionManager;
import main.java.ops.ocr.enums.DB;
import main.java.ops.ocr.enums.MeasurementGroup;

public class Measure {
	private static final String measurementsTable ="qMeasurement";
	private static final String pMeasurementsTable ="rap_measurement";
	private static final String qryCreateMeasurementTable = "CREATE TABLE `"+measurementsTable+"` (\n" + 
			"  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,\n"+
			"  `store_name` varchar(50) DEFAULT NULL,\n" +
			"  `measurement_id` int(11) DEFAULT NULL,\n" + 
			"  `metric` varchar(50) DEFAULT NULL,\n" + 
			"  `item_count` int(11) DEFAULT NULL,\n" + 
			"  `meta` varchar(50) DEFAULT NULL,\n" +
			"  `mgroup` varchar(50) DEFAULT NULL,\n" +
			"  `truth_item_count` int(11) DEFAULT NULL,\n" +  
			"  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" + 
			"  PRIMARY KEY (`id`)\n" + 
			") ENGINE=InnoDB DEFAULT CHARSET=latin1;";
	private final ConnectionManager cm;
	private final boolean persistToMeasurementsDb;
	private final MeasurementGroup mg;
	private final List<String> retailers;
	private int measurementId;
	public Measure(ConnectionManager cm,boolean persistToMeasurementsDb,List<String> retailers,MeasurementGroup mg){
		this.cm=cm;
		this.retailers=retailers;
		this.mg=mg;
		this.persistToMeasurementsDb=persistToMeasurementsDb;
		ResultSet rs = cm.getSelect("max(measurement_id)", "rap_measurement", null, null, null, DB.P_REPORTDB);
		try {
			rs.next();
			this.measurementId = rs.getInt(1)+1;
			cm.createTable(qryCreateMeasurementTable);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	protected void measureICAlgDistribution() {
		getMGWCodedItemCount();
		String select = this.measurementId + " measurement_id,'IC_Distribution' metric,count(distinct qri.id) item_count,if(qri.product_alg is null,'MISS',if(prri.id is null,concat('INVALID-',qri.product_alg),qri.product_alg)) meta,'"+mg.toString()+"' mgroup,(select qm.item_count from qMeasurement qm where qm.metric='IC_MGW_Coded_Item_Count' and qm.store_name=s_name) truth_item_count";
        String from = "qri join qr on qr.id=qri.receipt_id join qrr on qrr.ref_id=qr.ref_id left join prr on prr.ref_id=qr.ref_id left join prri on prri.raw_receipt_id=prr.id and trim(leading '0' from qri.code)=trim(leading '0' from prri.code)";
		String where = null;
        String groupBy = "s_name,meta";
		if (retailers!=null){
			select = createCase("qrr.store_name","s_name") + select;
			groupBy = groupBy + " having s_name in "+
					retailers.stream().collect(Collectors.joining("','", "('", "')"));
		}
		else
		{
			select = "qrr.store_name s_name,"+select;
		}
		String orderBy = "s_name,meta";
		DB db = DB.LOCAL;
		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
		
		System.out.println("\nIC algorithm distribution in QA:" );
		cm.printResultSet(rs);
		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
		if(persistToMeasurementsDb)
			cm.persistMeasurement(rs, DB.P_REPORTDB,pMeasurementsTable);

		System.out.println("\nItem coding algorithm distribution in Production is not yet available.\nItem coding success breakdown shown instead:" );
		cm.printResultSet(cm.getSelect("count(distinct pri.id) item_count,pri.product_id is null `product_id missing`", "pri", null, "product_id is null",null, DB.LOCAL));

	}
	protected void getMGWCodedItemCount() {
		String select = this.measurementId + " measurement_id,'IC_MGW_Coded_Item_Count' metric, count(distinct prri.`id`) item_count,'"+mg.toString()+"' mgroup";
		String from = "prri join prr on prr.id=prri.raw_receipt_id join qrr on qrr.ref_id=prr.ref_id ";
		String where = "prri.code regexp '^0+$' !=1 is true";
		String groupBy = "prr.store_name";
		if (retailers!=null){
			groupBy =retailers.stream().collect(Collectors.joining("%',prr.store_name like '%", "prr.store_name like '%", "%'"));
			select = createCase("prr.store_name","store_name") + select;
			where =where + retailers.stream().collect(Collectors.joining("%' or prr.store_name like '%", " and (prr.store_name like '%", "%')"));		
		}else{
			select = "prr.store_name store_name,"+select;
		}
		String orderBy = "prr.store_name";
		DB db = DB.LOCAL;
		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
		
		System.out.println("\nIC MGW Coded item count:" );
		cm.printResultSet(rs);
		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
	}
	protected void measureTruthSetAccuracy() {
		String select = this.measurementId + " measurement_id,'IC_Distribution' metric,count(distinct qri.id) item_count,if(qri.product_alg is null,\"MISS\",qri.product_alg) meta,'"+mg.toString()+"' mgroup";
		String from = "qri join qr on qr.id=qri.receipt_id join qrr on qrr.ref_id=qr.ref_id";
		String where = null;
		String groupBy = null;
		if (retailers!=null){
			groupBy =retailers.stream().collect(Collectors.joining("%',store_name like '%", "store_name like '%", "%',meta"));
			select = createCase("store_name","store_name") + select;
		}else{
			groupBy ="store_name,meta";
			select = "store_name,"+select;
		}
		String orderBy = "store_name,meta";
		DB db = DB.LOCAL;
		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
		
		System.out.println("\nIC algorithm distribution in QA:" );
		cm.printResultSet(rs);
		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
		
		System.out.println("\nItem coding algorithm distribution in Production is not yet available.\nItem coding success breakdown shown instead:" );
		cm.printResultSet(cm.getSelect("count(distinct pri.id) item_count,pri.product_id is null `product_id missing`", "pri", null, "product_id is null",null, DB.LOCAL));

	}
	protected void measureTruthSetUPCMatch() {
		String select = this.measurementId + " measurement_id,'IC_Distribution' metric,count(distinct ti.id) item_count,if(qri.product_id is null,'Truthset_Not Matched','Truthset_Matched') meta,'"+mg.toString()+"' mgroup";
		String from = "truth.truth_item ti join coredb.product p on p.gtin=ti.barcode_upc join qr on qr.ref_id=ti.ref_id join qrr on qrr.ref_id=qr.ref_id left join qri on qri.receipt_id=qr.id and qri.product_id=p.id";
		String where = "ti.valid=1";
		String groupBy = null;
		if (retailers!=null){
			groupBy =retailers.stream().collect(Collectors.joining("%',store_name like '%", "store_name like '%", "%',meta"));
			select = createCase("store_name","store_name") + select;
		}else{
			groupBy ="store_name,meta";
			select = "store_name,"+select;
		}
		String orderBy = "store_name,meta";
		DB db = DB.LOCAL;
		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
		
		System.out.println("\nTruthset UPC match:" );
		cm.printResultSet(rs);
		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
		
	}
	protected void measureTruthSetBrandMatch() {
		String select = this.measurementId + " measurement_id,'TruthSet_Brand_Match' metric,count(distinct ti.id) item_count,if(qri.product_id is null,'Truthset_Not Matched','Truthset_Matched') meta,'"+mg.toString()+"' mgroup";
		String from = "";
		String where = "ti.valid=1";
		String groupBy = null;
		if (retailers!=null){
			groupBy =retailers.stream().collect(Collectors.joining("%',store_name like '%", "store_name like '%", "%',meta"));
			select = createCase("store_name","store_name") + select;
		}else{
			groupBy ="store_name,meta";
			select = "store_name,"+select;
		}
		String orderBy = "store_name,meta";
		DB db = DB.LOCAL;
		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
		
		System.out.println("\nTruthset UPC match:" );
		cm.printResultSet(rs);
		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
		
	}
	protected void measureCompAlgDistribution() {
		String select = this.measurementId + " measurement_id,store_name,count(distinct p_rid) cnt, match_type";
		String from = "qComparison join prr on qComparison.ref_id=prr.ref_id";
		String where = null;
		String groupBy = "match_type";
		String orderBy = "store_name,p_rid";
		DB db = DB.LOCAL;
		System.out.println("\nComparison algorithm distribution:" );
		cm.printResultSet(cm.getSelect(select, from, where, groupBy,orderBy, db));	
	}
	protected void measureOCRGarbage(){
		String select = this.measurementId + " measurement_id,'OCR_Garbage' metric,count(rip_id is null) item_count, if(rip_id is null,'Garbage','Not_garbage') meta,'"+mg.toString()+"' mgroup";
		String from = "PrightQ";
		String where = null;
		String groupBy = null;
		if (retailers!=null){
			groupBy =retailers.stream().collect(Collectors.joining("%',store_name like '%", "store_name like '%", "%',rip_id is null"));
			select = createCase("store_name","store_name") + select;
		}else{
			select = "store_name,"+select;
			groupBy ="store_name,rip_id is null";
		}
		String orderBy = "store_name,rip_id";
		DB db = DB.LOCAL;
		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
		System.out.println("\nOCR Garbage metrics:" );
		cm.printResultSet(rs);
		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
	}
	protected void measureOCRAccuracy() {
		String select = this.measurementId + " measurement_id,'OCR_Accuracy' metric,count(distinct rip_id) item_count ,if(ri_id is null,'MISS','HIT') meta,'"+mg.toString()+"' mgroup";
		String from = "PleftQ";
		String where = null;
		String groupBy = null;
		if (retailers!=null){
			groupBy =retailers.stream().collect(Collectors.joining("%',store_name like '%", "store_name like '%", "%',ri_id is null"));
			select = createCase("store_name","store_name") + select;
		}else{
			groupBy ="store_name,ri_id is null";
			select = "store_name,"+select;
		}
		String orderBy = "store_name";
		DB db = DB.LOCAL;
		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
		System.out.println("\nOCR Accuracy metrics:" );
		cm.printResultSet(rs);
		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
	}
	protected void measureGarbageNew()  {
		int gCount = 0;
		int qCount = 0 ;
		try {
			ResultSet rs = cm.getSelect("count(distinct id) cnt","qGarbage", null,null, null,DB.LOCAL);
			rs.next();
			gCount = rs.getInt(1);
			rs = cm.getSelect("count(distinct id) cnt","qrri", null,null, null,DB.LOCAL);
			rs.next();
			qCount = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (gCount==0 || qCount==0){
			System.out.println("\nOCR garbage measurement problem !!!:" );
		}else{
			System.out.println("\nOCR garbage:" );
			System.out.println("Garbage items: "+gCount );
			System.out.println("Total OCR items: "+qCount );
			DecimalFormat df = new DecimalFormat();
			df.setMaximumFractionDigits(2);
			System.out.println("Garbage ratio: "+df.format((((double)gCount)/qCount)*100)+"%");
		}
	}
	public void measureGroupMetrics() {
		//TODO: insert logic per Group here.
		String select = "'Group_IC' metric,if(meta in ('TEXT','MISS'),'FAIL','SUCCESS') meta,sum(item_count) `item_count`,'"+mg.toString()+"' mgroup";
		String from = measurementsTable+" qm";
		String where = "metric='IC_Distribution'";
		String groupBy = "meta in ('TEXT','MISS')";
		String orderBy = null;
		DB db = DB.LOCAL;
		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
		
		System.out.println("\nGroup measurement results:" );
		cm.printResultSet(rs);
		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
		
	}
	public void measureRetailerRecognition(){
		String select =  this.measurementId + " measurement_id,'Retailer_Recognition' metric,count(distinct prr.id) item_count,if(qrrid is null,'Not matched','Matched') meta";
		String from = "prr left join(select qrr.id qrrid,qrr.ref_id from qrr  ";
		String where = null;
		String groupBy = null;
		if (retailers!=null){
			String innerWhere ="where "+retailers.stream().collect(Collectors.joining("%' or store_name like '%", "store_name like '%", "%'"));
			from = from + innerWhere;
			select = createCase("store_name","store_name") + select;
			groupBy =retailers.stream().collect(Collectors.joining("%',store_name like '%", "store_name like '%", "%',meta"));
		}else{
			select = "store_name,"+ select;
			groupBy ="store_name,meta";
		}
		from = from+") qrr on prr.ref_id=qrr.ref_id ";
		String orderBy = null;
		DB db = DB.LOCAL;
		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
		
		System.out.println("\nGroup measurement results:" );
		cm.printResultSet(rs);
		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
		
	}
	public void measureTotal(){
		String select = this.measurementId + " measurement_id,'OCR_Accuracy_Receipt_Total' metric,count(distinct qrr.id) item_count,\n" + 
				"if((qrr.total is null or prr.total!=qrr.total) and prr.total is not null,\"Not matched\",'Matched') meta,'"+mg.toString()+"' mgroup";
		String from = "prr join qrr on prr.ref_id=qrr.ref_id";
		String where = null;
		String groupBy = null;
		if (retailers!=null){
			groupBy =retailers.stream().collect(Collectors.joining("%',prr.store_name like '%", "prr.store_name like '%", "%',meta"));
			select = createCase("prr.store_name","store_name") + select;
		}else{
			groupBy ="store_name,meta";	
			select = "prr.store_name,"+ select;
		}
		String orderBy = null;
		DB db = DB.LOCAL;
		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
		
		System.out.println("\nReceipt Total measurements:" );
		cm.printResultSet(rs);
		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
		
	}
	public void measureAddress(){
		String select = this.measurementId + " measurement_id,'OCR_Accuracy_Receipt_Address' metric,count(distinct qrr.id) item_count,if(`ocr-us`.levenshtein(prr.address,`ocr-us`.DELETE_DOUBLE_SPACES(qrr.address))/char_length(if(char_length(prr.address)>char_length(qrr.address),prr.address,qrr.address))>0.72 or qrr.address is null,'Not matched','Matched') meta,'"+mg.toString()+"' mgroup";
		String from = "prr join qrr on prr.ref_id=qrr.ref_id";
		String where = "prr.address is not null";
		String groupBy = null;
		if (retailers!=null){
			groupBy =retailers.stream().collect(Collectors.joining("%',prr.store_name like '%", "prr.store_name like '%", "%',meta"));
			select = createCase("prr.store_name","store_name") + select;
		}else{
			groupBy ="store_name,meta";	
			select = "prr.store_name,"+ select;
		}
		String orderBy = null;
		DB db = DB.LOCAL;
		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
		
		System.out.println("\nReceipt Address measurements:" );
		cm.printResultSet(rs);
		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
	}
	public void measureDate(){
		String select = this.measurementId + " measurement_id,'OCR_Accuracy_Receipt_Date' metric,count(distinct qrr.id) item_count,\n" + 
				"if((qrr.purchase_date is null or prr.purchase_date!=qrr.purchase_date) and prr.purchase_date is not null,'Not matched','Matched') meta,'"+mg.toString()+"' mgroup";
		String from = "prr join qrr on prr.ref_id=qrr.ref_id";
		String where = null;
		String groupBy = null;
		if (retailers!=null){
			groupBy =retailers.stream().collect(Collectors.joining("%',prr.store_name like '%", "prr.store_name like '%", "%',meta"));
			select = createCase("prr.store_name","store_name") + select;
		}else{
			groupBy ="store_name,meta";	
			select = "prr.store_name,"+ select;
		}
		String orderBy = null;
		DB db = DB.LOCAL;
		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
		
		System.out.println("\nReceipt totals measurements:" );
		cm.printResultSet(rs);
		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
	}
	public void measureTime(){
		String select = this.measurementId + " measurement_id,'OCR_Accuracy_Receipt_Time' metric, count(distinct qrr.id)item_count,if(prr.purchase_time is not null and (qrr.purchase_time is null or TIME_FORMAT(if(instr(prr.purchase_time,'PM')>0 and hour(prr.purchase_time)!=12,addtime(time(prr.purchase_time),'12:00'),time(prr.purchase_time)) ,'%H:%i')!=TIME_FORMAT(time(qrr.purchase_time),'%H:%i')),'Not Matched','Matched') meta,'"+mg.toString()+"' mgroup";
		String from = "prr join qrr on prr.ref_id=qrr.ref_id";
		String where = null;
		String groupBy = null;
		if (retailers!=null){
			groupBy =retailers.stream().collect(Collectors.joining("%',prr.store_name like '%", "prr.store_name like '%", "%',meta"));
			select = createCase("prr.store_name","store_name") + select;
		}else{
			groupBy ="store_name,meta";	
			select = "prr.store_name,"+ select;
		}
		String orderBy = null;
		DB db = DB.LOCAL;
		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
		
		System.out.println("\nReceipt totals measurements:" );
		cm.printResultSet(rs);
		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
	}
	public void measureTelephone(){
		String select = this.measurementId + " measurement_id,'OCR_Accuracy_Receipt_Telephone' metric,count(distinct qrr.id) item_count,\n" + 
				"if((qrr.store_phone is null or prr.store_phone!=qrr.store_phone) and prr.store_phone is not null,\"Not matched\",'Matched') meta,'"+mg.toString()+"' mgroup";
		String from = "prr join qrr on prr.ref_id=qrr.ref_id";
		String where = null;
		String groupBy = null;
		if (retailers!=null){
			groupBy =retailers.stream().collect(Collectors.joining("%',prr.store_name like '%", "prr.store_name like '%", "%',meta"));
			select = createCase("prr.store_name","store_name") + select;
		}else{
			groupBy ="store_name,meta";	
			select = "prr.store_name,"+ select;
		}
		String orderBy = null;
		DB db = DB.LOCAL;
		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
		
		System.out.println("\nReceipt Telephone measurements:" );
		cm.printResultSet(rs);
		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
	}
	public void measureStoreIdFound(){
		String select = this.measurementId + " measurement_id,'IC_Store_Identification' metric,count(distinct qr.id) item_count,'Store IDs found' meta,'"+mg.toString()+"' mgroup";
		String from = "qr";
		String where = "store_id is not null";
		String groupBy = null;
		if (retailers!=null){
			groupBy =retailers.stream().collect(Collectors.joining("%',prr.store_name like '%", "prr.store_name like '%", "%',meta"));
			select = createCase("prr.store_name","store_name") + select;
		}else{
			groupBy ="store_name,meta";	
			select = "prr.store_name,"+ select;
		}
		String orderBy = null;
		DB db = DB.LOCAL;
		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
		
		System.out.println("\nReceipt Store IDs Found measurements:" );
		cm.printResultSet(rs);
		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
	}
	public void measureItemPrice(){
		String select = this.measurementId + " measurement_id,'OCR_Accuracy_Item_Price' metric,count(distinct `rip_id`) item_count,if(p_total=total is true,'Matched','Not Matched') meta,'"+mg.toString()+"' mgroup";
		String from = "pleftq";
		String where = "p_total is not null";
		String groupBy = null;
		if (retailers!=null){
			select = createCase("store_name","store_name") + select;
			groupBy =retailers.stream().collect(Collectors.joining("%',store_name like '%", "store_name like '%", "%',meta"));
		}else{
			select = "store_name,"+ select;
			groupBy ="store_name,meta";
		}
		String orderBy = null;
		DB db = DB.LOCAL;
		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
		
		System.out.println("\nReceipt Item price measurements:" );
		cm.printResultSet(rs);
		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
	}
	public void measureItemQty(){
		String select = this.measurementId + " measurement_id,'OCR_Accuracy_Item_Qty' metric,count(distinct `rip_id`) item_count,if(p_qty=qty is true,'Matched','Not Matched') meta,'"+mg.toString()+"' mgroup";
		String from = "pleftq";
		String where = "p_qty is not null";
		String groupBy = null;
		if (retailers!=null){
			select = createCase("store_name","store_name") + select;
			groupBy =retailers.stream().collect(Collectors.joining("%',store_name like '%", "store_name like '%", "%',meta"));
		}else{
			select = "store_name,"+ select;
			groupBy ="store_name,meta";			
		}
		String orderBy = null;
		DB db = DB.LOCAL;
		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
		
		System.out.println("\nReceipt Item qty measurements:" );
		cm.printResultSet(rs);
		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
	}
	public void measureAdapterDistribution() {
//		String select = this.measurementId + " measurement_id,'Group_Adapter_Distribution' metric,count(distinct `rip_id`) item_count,if(p_qty=qty is true,'Matched','Not Matched') meta,'"+mg.toString()+"' mgroup";
//		String from = "pleftq";
//		String where = "p_qty is not null";
//		String groupBy = null;
//		if (retailers!=null){
////			select = createCase("store_name","store_name") + select;
//			groupBy =retailers.stream().collect(Collectors.joining("%',store_name like '%", "store_name like '%", "%',meta"));
//		}else{
//			select = "store_name,"+ select;
//			groupBy ="store_name,meta";			
//		}
//		String orderBy = null;
//		DB db = DB.LOCAL;
//		ResultSet rs = cm.getSelect(select, from, where, groupBy,orderBy, db);
//		
//		System.out.println("\nReceipt Item qty measurements:" );
//		cm.printResultSet(rs);
//		cm.persistMeasurement(rs, DB.LOCAL,measurementsTable);
//		if(persistToMeasurementsDb)
//			cm.persistMeasurement(rs, DB.P_REPORTDB,pMeasurementsTable);
		
	}
	private String createCase(String field, String endAs) {
		StringBuilder sb = new StringBuilder();
		sb.append("case ");
		for(String retailer:retailers){
			sb.append("when "+field+" like '%"+retailer+"%' then '"+retailer+"' ");
		}
		sb.append("end as "+endAs+" ,");
		return sb.toString();
	}
	
}