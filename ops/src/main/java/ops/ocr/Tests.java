package main.java.ops.ocr;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Test;
import main.java.ops.ocr.enums.CreatorType;
import main.java.ops.ocr.enums.DB;
import main.java.ops.ocr.enums.MeasurementGroup;
import main.java.ops.ocr.enums.Table;

public class Tests {
	public static MeasurementGroup mg = MeasurementGroup.Target;
	public static boolean persistToMeasurementsDb = true;
	public static String retailersPath = "res/groups/A.retailers";
	public static String refsList = "res/ref-sets/TW-50.refs";
	private static String batchTimestamp = "_2017_04_13_14_42_17";
	public static List<String> retailers;
	public static String singleRetailerName = "Walmart";
	public static int size = 10;
	public static CreatorType creator = CreatorType.MGW;
	public static String requester = "AVI";
	public static HashMap<String,String> configs= new HashMap<String,String>();
	public static final double levThreshold=0.5;
	public static String upperDateLimit = "2016-09-20";//"2017-01-26";
	private static final Table[] allTables = new Table[]{Table.raw_receipt,Table.raw_item,Table.receipt,Table.receipt_item,Table.snapshot};
	private US initUS(US us, String rName,String batchTimestamp) {
		String refsPath = "res/copied-refs/"+batchTimestamp+rName+".txt";
		return new US(refsPath,rName,requester);
	}
	@Test
	public void sendSingleRetailer(){
		US us = new US(singleRetailerName,size,creator,upperDateLimit,requester);
		us.send();
		us.consolidate(DB.P_RIS,allTables);
		us.close();
	}
	@Test
	public void sendByRetailerList() throws IOException{
		Stream<String> lines = Files.lines(Paths.get(retailersPath));
		retailers = lines.collect(Collectors.toList());
		lines.close();
		US us = null;
		for(String retailerName:retailers){
			us = new US(retailerName,size,creator,upperDateLimit,requester);
			us.send();
		}
		us.consolidate(DB.P_RIS,allTables);
		us.close();
	}
	@Test
	public void sendNewGroup() throws IOException{
		Stream<String> lines = Files.lines(Paths.get("res/group"+mg.toString()+".retailers"));
		retailers = lines.collect(Collectors.toList());
		lines.close();
		US us = null;
		System.out.println("Sending by group "+mg.toString());
		for(String retailerName:retailers){
			us = new US(retailerName,size,creator,upperDateLimit,requester);
			us.send();
		}
		us.consolidate(DB.P_RIS,allTables);
		us.close();
	}
	@Test
	public void sendPreparedGroup() throws IOException{
		US us = new US("res/ref-sets/"+mg.toString()+".refs",mg.toString(),requester);
		System.out.println("Sending by group "+mg.toString());
		us.send();
		us.consolidate(DB.P_RIS,allTables);
		us.close();
	}
	@Test
	public void sendByRefsList() {
		US us = new US(refsList,singleRetailerName,requester);
		us.send();
		us.consolidate(DB.P_RIS,allTables);
		us.close();
	}
	
	@Test
	public void sendByDate() {
		US us = new US(singleRetailerName,size,upperDateLimit,requester);
		us.send();
		us.consolidate(DB.P_RIS,allTables);
		us.close();
	}
	@Test
	public void receiveLastSent() {
		US us = new US("res/copied-refs/last.txt",singleRetailerName,requester);
		us.receive();
		consolidateCompareAndMeasure(us,null);
		us.close();
		System.out.println("All done");
	}
	@Test
	public void receiveByRetailerList() throws IOException {
		Stream<String> lines = Files.lines(Paths.get(retailersPath));
		retailers = lines.collect(Collectors.toList());
		lines.close();
		US us = null;
		for(String rName:retailers){
			us = initUS(us,rName,batchTimestamp);
			us.receive();
		}
		consolidateCompareAndMeasure(us,retailers);
		us.close();
	}
	@Test
	public void receiveByRefsList() {
		US us = new US(refsList,singleRetailerName,requester);
		us.receive();
		consolidateCompareAndMeasure(us,null);
		us.close();
	}
	@Test
	public void receiveNewGroup() throws IOException {
		Stream<String> lines = Files.lines(Paths.get("res/group"+mg.toString()+".retailers"));
		retailers = lines.collect(Collectors.toList());
		lines.close();
		US us = null;
		for(String rName:retailers){
			us = initUS(us,rName,batchTimestamp);
			us.receive();
		}
		consolidateCompareAndMeasure(us,retailers);
		us.close();
	}
	@Test
	public void receivePreparedGroup() throws IOException {
		Stream<String> lines = Files.lines(Paths.get("res/groups/"+mg.toString()+".retailers"));
		retailers = lines.collect(Collectors.toList());
		lines.close();
		US us = new US("res/ref-sets/"+mg.toString()+".refs",mg.toString(),requester);
		us.receive();
		consolidateCompareAndMeasure(us,retailers);
		us.close();
	}
	@Test
	public void measure() throws IOException{
		Stream<String> lines = Files.lines(Paths.get(retailersPath));
		retailers = lines.collect(Collectors.toList());
		lines.close();
		US us = null;
		for(String rName:retailers){
			us = initUS(us,rName,batchTimestamp);
			break;
		}
		us.performMeasurements(retailers,mg,persistToMeasurementsDb);
	}
	@Test
	public void createOCRComparison(){
		US us = new US("res/copied-refs/last.txt",singleRetailerName,requester);
		us.createOCRComparisonTables();
		us.close();
	}
	public void clearQA(){
		US us = new US("res/copied-refs/last.txt",singleRetailerName,requester);
		us.clearQARISAndMturk();
		us.close();
	}
	private void consolidateCompareAndMeasure(US us,List<String> retailers) {
		System.out.println("\nConsolidating results from different retailers.\n");
		us.consolidate(DB.Q_RIS,allTables);
		System.out.println("Starting OCR accuracy comparisons...");
		us.smartCompare(levThreshold);
		us.createOCRComparisonTables();
		us.performMeasurements(retailers,mg,persistToMeasurementsDb);
	}
	
}