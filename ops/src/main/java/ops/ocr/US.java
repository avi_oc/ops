package main.java.ops.ocr;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

import il.co.zollo.framework.domain.raw.RawItem;
import main.java.ops.ConnectionManager;
import main.java.ops.ocr.enums.CreatorType;
import main.java.ops.ocr.enums.DB;
import main.java.ops.ocr.enums.MeasurementGroup;
import main.java.ops.ocr.enums.Table;
public class US {
	private static final String file_suffix = ".bash";
	private static final String script_path = "res/scripts/";
	private final String selectSet = "select snapshot.ref_id from risdb.snapshot join risdb.raw_receipt on raw_receipt.ref_id=snapshot.ref_id where (raw_receipt.creator='{creator}' or raw_receipt.creator is null) and raw_receipt.store_name like \"%{name}%\" and snapshot.created_on < '{date}' order by snapshot.id desc limit {size}";
	private final String selectSetByDate = "select snapshot.ref_id from risdb.snapshot join risdb.raw_receipt on raw_receipt.ref_id=snapshot.ref_id where (raw_receipt.creator='{creator}' or raw_receipt.creator is null) and snapshot.created_on < '{date}' order by snapshot.id desc limit {size}";
	private final String query = "select `{table}`.* from risdb.snapshot join risdb.raw_receipt on raw_receipt.ref_id=snapshot.ref_id left join risdb.receipt on receipt.ref_id=snapshot.ref_id where snapshot.ref_id in({refs})";
	private final String qryRawItems = "select `{table}`.* from risdb.raw_item join risdb.raw_receipt on raw_receipt.id=raw_item.raw_receipt_id where raw_receipt.ref_id in({refs})";
	private final String qryRISItems = "select `{table}`.* from risdb.receipt_item join risdb.receipt on receipt.id=receipt_item.receipt_id where receipt.ref_id in({refs})";
	private final String qryDeleteByRefs = "delete from {from} where ref_id in({refs})";
	private final String qrySQSMessagesByRefs ="select concat('aws --profile OC --region us-east-1 sqs send-message --queue-url https://sqs.us-east-1.amazonaws.com/867154987980/qa_ris_snapshot --message-body \"{\\\\\"url\\\\\" : \\\\\"',s.`url`,'\\\\\",\\\\\"country\\\\\":\\\\\"US\\\\\"}\" --message-attributes \\\"sender_id={\"DataType\"=String,\"StringValue\"=\"{requester}\"},ref_id={\"DataType\"=String,\"StringValue\"=\"',s.`ref_id`,'\"},requester_id={\"DataType\"=String,\"StringValue\"=\"{requester}\"},content_type={\"DataType\"=String,\"StringValue\"=\"Snapshot\"}\\\";') message_command from `snapshot` s where s.ref_id in({refs})"; 
	private final String qryGarbage = "create table qGarbage select s.url,s.ref_id,qrr.store_name,q.*\n" + 
			"from qrri q\n" + 
			"join comparison_garbage cg on q.id=cg.`ri_id`\n" + 
			"join qrr on qrr.id=q.raw_receipt_id\n" + 
			"join qs s on s.ref_id=qrr.ref_id\n" + 
			"order by qrr.id,q.id;";
	private final String qryComparison = "create table qComparison select prr.ref_id,s.url,p.id p_rid,p.raw_receipt_id p_rrid,p.code p_code,p.qty p_qty,p.total p_total,cr.*,qrri.*\n" + 
			"from prri p\n" + 
			"left join comparison_results cr on p.id=cr.`source_ri_id`\n" + 
			"left join qrri on qrri.id=cr.`target_ri_id`\n" + 
			"left join prr on prr.id=p.raw_receipt_id\n" + 
			"left join qrr on qrr.id=qrri.raw_receipt_id\n" + 
			"left join ps s on s.ref_id=prr.ref_id\n" + 
			"order by prr.id,p.id;";
	private final String qryPleftQ = "create table PleftQ \n"+
			"select prod.*,`ocr-us`.levenshtein(prod.p_desc,ocr.desc)/char_length(prod.p_desc) relevance,if(prod.p_code=ocr.code and ocr.code is not null,'CODE',if(`desc` is not null and p_desc is not null,'LEV','MISS')) comp_alg,ocr.*\n" + 
			"from \n" + 
			"(	select rip.id rip_id,`desc` p_desc,`code` p_code,qty p_qty,rip.total p_total,rrp.ref_id p_ref_id,s.url,rrp.store_name\n" + 
			"	from prr rrp\n" + 
			"	inner join ps s on s.ref_id=rrp.ref_id\n" + 
			"	inner join qrr rr on rr.ref_id=rrp.ref_id\n" + 
			"	inner join prri rip on rip.raw_receipt_id=rrp.id\n" + 
			" 	) prod\n" + 
			"left join # left join shows missing items\n" + 
			"(	select ri.id ri_id,`desc`,`code`,qty,ri.total,rr.ref_id\n" + 
			"	from qrr rr \n" + 
			"	inner join qrri ri on ri.raw_receipt_id=rr.id\n" + 
			"	) ocr on rip_id!=ri_id and prod.p_ref_id=ocr.ref_id and (prod.p_code=ocr.code or `ocr-us`.levenshtein(prod.p_desc,`ocr-us`.DELETE_DOUBLE_SPACES(ocr.desc))/char_length(prod.p_desc)<0.6)\n" + 
			"group by prod.rip_id\n" + 
			"order by p_ref_id,rip_id";
	private final String qryPrightQ = "create table PrightQ \n" + 
			"select prod.*,`ocr-us`.levenshtein(prod.p_desc,ocr.desc)/char_length(prod.p_desc) relevance,ocr.*\n" + 
			"from \n" + 
			"(	select rip.id rip_id,`desc` p_desc,`code` p_code,qty p_qty,rip.total p_total,rrp.ref_id p_ref_id,s.url\n" + 
			"	from prr rrp\n" + 
			"	inner join ps s on s.ref_id=rrp.ref_id\n" + 
			"	inner join qrr rr on rr.ref_id=rrp.ref_id\n" + 
			"	inner join prri rip on rip.raw_receipt_id=rrp.id\n" + 
			" 	) prod\n" + 
			"right join # right join shows garbage\n" + 
			"(	select ri.id ri_id,`desc`,`code`,qty,ri.total,rr.ref_id,rrp.store_name\n" + 
			"	from qrr rr \n" + 
			"	inner join qrri ri on ri.raw_receipt_id=rr.id\n" + 
			"	left join prr rrp on rrp.ref_id=rr.ref_id\n" + 
			"	) ocr on rip_id!=ri_id and prod.p_ref_id=ocr.ref_id and (prod.p_code=ocr.code or `ocr-us`.levenshtein(prod.p_desc,`ocr-us`.DELETE_DOUBLE_SPACES(ocr.desc))/char_length(prod.p_desc)<0.6)\n" + 
			"group by ocr.ri_id\n" + 
			"order by ref_id,ri_id;";
	private String retailerName ;
	private final String requester;
	private final String defaultRequester = "AVI";
	private List<String> refs ;
	private double levThreshold;
	private static ConnectionManager cm = new ConnectionManager();

	public US(){
		this.requester=defaultRequester;
	}
	public US(String retailerName,int size,CreatorType creator, String upperDateLimit,String requesterTag){
		this.requester=requesterTag;
		this.retailerName=retailerName;
		System.out.println("Getting "+creator.toString()+" refIDs for \""+retailerName+"\" with size "+size);
		refs = getRefsFromProd(retailerName, size,creator,upperDateLimit);
		System.out.println(refs.size()+" refs retrieved.");
	}

	public US(String refsPath,String retailerName,String requesterTag){
		this.requester=requesterTag;
		this.retailerName=retailerName;
		loadRefsSuccessful(refsPath);
	}
	public US(String batchName,int size,String dateLimit,String requesterTag){
		this.requester=requesterTag;
		this.retailerName=batchName;
		this.refs=getRefsByDate(dateLimit, size, CreatorType.MGW);

	}
	public void send() {
		try{
			getProductionData();// copyByName(DB.P_RIS,DB.LOCAL,retailerName);
			clearQARISAndMturk();
			sendToQA(retailerName);
			persistRefs(retailerName);
		}catch (SQLException se){
			se.printStackTrace();
		}
		//		return refs;
	}
	private void persistRefs(String retailerName) {
		try {
			System.out.println("Persisting refs to file...");
			Files.write(Paths.get("res/copied-refs/"+cm.namingTimestamp+retailerName+".txt"),refs);
			Files.write(Paths.get("res/copied-refs/last.txt"),refs);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void sendToQA(String retailerName) throws SQLException {

		Path path = prepareScript(retailerName);
		System.out.println("Sending batch to SQS-RIS...");
		runScript(path);
		System.out.print("Done.");

	}
	private boolean runScript(Path script) {
		File f = script.toFile();
		if(f.setExecutable(true, false)){
			try {
				new ProcessBuilder("open","-a","Terminal.app",f.getAbsolutePath()).start();
				return true;
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
		return false;
	}
	private Path prepareScript(String retailerName) throws SQLException {
		String query = qrySQSMessagesByRefs.replace("{requester}", requester);
		List<String> lines = cm.createScriptFromRefs(prepQueryWithRefs(refs, query),DB.P_RIS);
		try {
			Path p = Files.write(Paths.get(script_path+cm.namingTimestamp+retailerName+"_"+file_suffix),lines);
			p.toFile().setExecutable(true, false);
			Path p2 = Files.write(Paths.get(script_path+"last"+file_suffix),lines);
			p2.toFile().setExecutable(true, false);
			return p;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void clearQARISAndMturk() {
		String deleteQuery = prepQueryWithRefs(refs, qryDeleteByRefs);
		DB db = DB.Q_RIS;
		System.out.println("Clearing QA DB before copying batch...");
		try {
			cm.deleteByRefs(deleteQuery,db,"snapshot");
			cm.deleteByRefs(deleteQuery,db,"raw_receipt");
			cm.deleteByRefs(deleteQuery,db,"receipt");
			cm.deleteByRefs(deleteQuery,db,"raw_receipt_failure");
			cm.deleteByRefs(deleteQuery,db,"mturkdb.hit_creation");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	//	private void copyByName(DB from,DB to,String retailerName) throws SQLException {
	//		System.out.println("copying snapshots");
	//		cm.copy(prepQueryWithRefs(refs,query), from, to,"snapshot",retailerName);
	//		System.out.println("copying raw receipts");
	//		cm.copy(prepQueryWithRefs(refs,query), from, to,"raw_receipt",retailerName);
	//		System.out.println("copying items");
	//		cm.copy(prepQueryWithRefs(refs,qryRawItems), from, to,"raw_item",retailerName);
	//	}
	//	private void copyRISItemsByRefs(DB from, DB to, List<String> refs, String retailerName) throws SQLException {
	//		System.out.println("copying Receipts...");
	//		cm.copy(prepQueryWithRefs(refs,query), from, to,"receipt",retailerName);
	//		System.out.println("copying Receipt Items...");
	//		cm.copy(prepQueryWithRefs(refs,qryRISItems), from, to,"receipt_item",retailerName);
	//		
	//	}
	private void copyByRefs(DB from,DB to,List<String> refs,String retailerName) throws SQLException {
		System.out.println("copying Snapshots...");
		cm.copy(prepQueryWithRefs(refs,query), from, to,"snapshot",retailerName);
		System.out.println("copying Raw Receipts...");
		cm.copy(prepQueryWithRefs(refs,query), from, to,"raw_receipt",retailerName);
		System.out.println("copying Raw Items...");
		cm.copy(prepQueryWithRefs(refs,qryRawItems), from, to,"raw_item",retailerName);
		System.out.println("copying Receipts...");
		cm.copy(prepQueryWithRefs(refs,query), from, to,"receipt",retailerName);
		System.out.println("copying Receipt Items...");
		cm.copy(prepQueryWithRefs(refs,qryRISItems), from, to,"receipt_item",retailerName);
	}
	private String prepSelectSet(int size, String name,CreatorType creator, String upperDateLimit)	{
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date d = new Date();
		String today = df.format(d);
		return selectSet.replace("{name}", name).replace("{size}", String.valueOf(size)).replace("{creator}", creator.toString()).replace("{date}", upperDateLimit==null?today:upperDateLimit);
	}
	private String prepSelectSetByDate(int size, String date,CreatorType creator)	{
		return selectSetByDate.replace("{date}", date).replace("{size}", String.valueOf(size)).replace("{creator}", creator.toString());
	}
	private String prepQueryWithRefs(List<String> refs,String query) {
		StringBuilder sb = new StringBuilder(1024);
		for(String ref:refs){
			sb.append("\"").append(ref).append("\",");
		}
		sb.deleteCharAt(sb.length()-1);
		return query.replace("{refs}",sb.toString());
	}
	public void receive(){
		try {
			copyByRefs(DB.Q_RIS, DB.LOCAL, refs, retailerName);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public boolean loadRefsSuccessful(String path) {
		if (this.refs!=null){
			System.out.println("Refs already loaded, use a new object. .");
			return false;
		}
		try (Stream<String> lines = Files.lines(Paths.get(path))) {
			this.refs= lines.collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;		
	}
	public void getProductionData() {
		try {
			copyByRefs(DB.P_RIS, DB.LOCAL, refs, retailerName);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	private List<String> getRefsByDate(String dateLimit, int size,CreatorType creator){
		try {
			return cm.getRefs(prepSelectSetByDate(size,dateLimit,creator),DB.P_RIS);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	private List<String> getRefsFromProd(String retailerName, int size,CreatorType creator,String upperDateLimit) {
		try {
			return cm.getRefs(prepSelectSet(size,retailerName,creator,upperDateLimit),DB.P_RIS);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public void consolidate(DB db,Table[] tables){
		try {
			cm.consolidateResults(db,tables);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void printResults(){
		HashMap<String,String> results = cm.getResults();
		for(String key:results.keySet()){
			System.out.println(key+" : "+results.get(key));
		}
	}
	public void close() {
		cm.close();
	}
	public void createOCRComparisonTables(){
		System.out.print("Creating matching items table PleftQ...." );
		cm.createUpdate(qryPleftQ);
		System.out.println("done.");
		System.out.print("Creating garbage items table PrightQ...." );
		cm.createUpdate(qryPrightQ);
		System.out.println("done.");
	}

	public void smartCompare(double levThreshold) {
		this.levThreshold = levThreshold;
		System.out.print("Creating comparison results...");
		HashMap<Integer,List<Object>> comparisonResults = new HashMap<Integer,List<Object>>(); //raw item id TO matched RawItem and match type (string)
		List<RawItem> garbageItems = new ArrayList<RawItem>();
		for(String ref:this.refs){
			List<RawItem> pItems = new ArrayList<RawItem>();
			List<RawItem> qItems = new ArrayList<RawItem>();
			prepItemLists(pItems,qItems,ref);
			for(RawItem pri:pItems){
				List<Object> matchResult = findMatch(pri,qItems);
				if(matchResult!=null){
					comparisonResults.put((Integer)pri.getId(), (List<Object>)matchResult);
					qItems.remove((RawItem)matchResult.get(0));
				}
			}
			garbageItems.addAll(qItems);
		}
		List<String> matches = new ArrayList<String>();
		for(Integer sId :comparisonResults.keySet()){
			Integer tId = (Integer)((RawItem)comparisonResults.get(sId).get(0)).getId();
			String matchType = (String)comparisonResults.get(sId).get(1);
			matches.add("("+sId+","+tId+",'"+matchType+"')");
		}
		List<String> garbage = new ArrayList<String>();
		for(RawItem gi:garbageItems){
			garbage.add("("+((Integer)gi.getId()).toString()+")");
		}
		try {
			cm.persistComparison(matches,qryComparison);
			cm.persistGarbage(garbage,qryGarbage);
			System.out.println("Done.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	private List<Object> findMatch(RawItem sItem, List<RawItem> tItems) {
		SortedMap<Double,RawItem> scores = new TreeMap<Double,RawItem>();
		for(RawItem tItem:tItems){
			if(Objects.nonNull(sItem.getCode()) && Objects.nonNull(tItem.getCode()))
				if (sItem.getCode().equals(tItem.getCode())) 
					return new ArrayList<Object>(Arrays.asList(tItem,"CODE")); 
			if(Objects.nonNull(sItem.getDesc()) && Objects.nonNull(tItem.getDesc()))
				scores.put(levenshteinRatio(sItem,tItem),tItem);
		}
		if(!scores.isEmpty()){
			if(scores.firstKey()<levThreshold)
				return new ArrayList<Object>(Arrays.asList(scores.get(scores.firstKey()),"LEV"));
		}
		return null;
	}


	private Double levenshteinRatio(RawItem sItem, RawItem tItem) {
		String sDesc = sItem.getDesc();
		String tDesc = tItem.getDesc();
		return LevenshteinDistance.computeLevenshteinDistance(sDesc, tDesc)/(double)(Math.max(sDesc.length(), tDesc.length()));

	}
	private void prepItemLists(List<RawItem> pItems,List<RawItem> qItems,String ref) {
		ResultSet rsp = cm.getSelect("*","Prri join Prr on Prr.id=Prri.raw_receipt_id", "ref_id='"+ref+"'",null, null,DB.LOCAL);
		ResultSet rsq = cm.getSelect("*","Qrri join Qrr on Qrr.id=Qrri.raw_receipt_id", "ref_id='"+ref+"'",null, null,DB.LOCAL);
		try {
			while (rsp.next()) {
				RawItem ri = new RawItem();
				ri.setCode(rsp.getString("code"));
				ri.setDesc(rsp.getString("desc"));
				ri.setId(rsp.getInt("id"));
				pItems.add(ri);
			}
			while (rsq.next()){
				RawItem ri = new RawItem();
				ri.setCode(rsq.getString("code"));
				ri.setDesc(rsq.getString("desc"));
				ri.setId(rsq.getInt("id"));
				qItems.add(ri);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void performMeasurements(List<String> retailers, MeasurementGroup mg,boolean persistToMeasurementsDb) {
		Measure m = new Measure(cm,persistToMeasurementsDb,retailers,mg);
		m.measureOCRAccuracy();
		m.measureOCRGarbage();
		m.measureRetailerRecognition();
		m.measureTotal();
		m.measureTime();
		m.measureDate();
		m.measureTelephone();
		m.measureItemPrice();
		m.measureItemQty();
		m.measureGarbageNew();
		m.measureICAlgDistribution();
//		m.getMGWCodedItemCount();
		m.measureGroupMetrics();
		m.measureCompAlgDistribution();
//		m.measureAdapterDistribution();
		m.measureTruthSetUPCMatch();
	}
	

}
