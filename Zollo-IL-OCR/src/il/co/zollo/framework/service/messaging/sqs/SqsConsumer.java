package il.co.zollo.framework.service.messaging.sqs;

import il.co.zollo.framework.domain.messaging.Envelope;
import il.co.zollo.framework.service.messaging.HandlerResult;
import il.co.zollo.framework.service.messaging.MessageConsumer;
import il.co.zollo.framework.service.messaging.MessageHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Function;

import org.apache.commons.configuration.MapConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.DeleteMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;

@Component
public class SqsConsumer implements MessageConsumer {

	private static final Logger logger = LoggerFactory.getLogger(SqsConsumer.class);

	@Autowired
	private AmazonSQS sqs;

	private final Function<Message, DeleteMessageBatchRequestEntry> messageToReceiptHandleFn = new Function<Message, DeleteMessageBatchRequestEntry>() {
		public DeleteMessageBatchRequestEntry apply(Message t) {
			return new DeleteMessageBatchRequestEntry(t.getMessageId(), t.getReceiptHandle());
		}
	};

	@Async
	@Override
	public void consume(MapConfiguration config) {
		MessageHandler handler = (MessageHandler) config.getProperty(MESSAGE_HANDLER_PROP_KEY);
		ReceiveMessageRequest receiveMessageReq = new ReceiveMessageRequest().withQueueUrl(config.getString(QUEUE_PROP_KEY))
				.withWaitTimeSeconds(config.getInteger(ARRIVE_WAIT_SECS_PROP_KEY, 20))
				.withVisibilityTimeout(config.getInteger(VISIBILITY_TIMEOUT_PROP_KEY, 30))
				.withMaxNumberOfMessages(config.getInteger(MAX_PER_POLL_PROP_KEY, 1))
				.withMessageAttributeNames(SENDER_ID_KEY, REF_ID_KEY, CONTENT_TYPE_KEY, REQUESTER_KEY, PRIORITY_KEY);

		while (true) {
			try {
				ReceiveMessageResult receiveResult = sqs.receiveMessage(receiveMessageReq);
				List<Message> messages = receiveResult.getMessages();

				if (messages.size() > 0) {
					logger.debug(new StringBuffer("Detected ").append(messages.size()).append(" SQS messages in queue ")
							.append(receiveMessageReq.getQueueUrl()).toString());
					List<HandlerResult> handlerResults = new ArrayList<>();

					// Send an envelope (constructed from message) to each
					// handler
					for (Message message : messages) {
						try {
							MessageAttributeValue senderIdAttr = message.getMessageAttributes().get(SENDER_ID_KEY);
							String senderId = null;
							String refId = null;
							String contentType = null;
							String requesterId = null;
							String priority = null;

							if (senderIdAttr != null) {
								senderId = senderIdAttr.getStringValue();

							} else {
								senderId = ANON_SENDER_ID;
							}
							MessageAttributeValue refIdAttr = message.getMessageAttributes().get(REF_ID_KEY);

							if (refIdAttr != null) {
								refId = refIdAttr.getStringValue();
							}
							MessageAttributeValue contentTypeAttr = message.getMessageAttributes().get(CONTENT_TYPE_KEY);

							if (contentTypeAttr != null) {
								contentType = contentTypeAttr.getStringValue();
							}
							MessageAttributeValue requesterIdAttr = message.getMessageAttributes().get(REQUESTER_KEY);

							if (requesterIdAttr != null) {
								requesterId = requesterIdAttr.getStringValue();
							}	
							
							MessageAttributeValue priorityAttr = message.getMessageAttributes().get(PRIORITY_KEY);

							if (priorityAttr != null) {
								priority = priorityAttr.getStringValue();
								
							} else {
								priority = "0";
							}						
							Envelope envelope = new Envelope().withSenderId(senderId).withRefId(refId).withPriority(Integer.valueOf(priority)).withRequesterId(requesterId).withMessage(message.getBody());

							if (contentType != null) {
								envelope.withContentType(contentType);
							}
							Future<Object> future = handler.handle(envelope);
							String handle = message.getReceiptHandle();
							handlerResults.add(new HandlerResult(future, handle));

						} catch (Exception e) {
							logger.error("Encountered pre-handler failure for " + message.toString(), e);
						}
					}

					// Delete messages from queue after handlers are done
					for (HandlerResult handlerResult : handlerResults) {
						while (!handlerResult.getFuture().isDone()) {
							sleep(2L);
						}

						try {
							handlerResult.getFuture().get();
							sqs.deleteMessage(new DeleteMessageRequest(config.getString(QUEUE_PROP_KEY), handlerResult.getHandle()));

						} catch (InterruptedException | ExecutionException e) {
							logger.error("Handler reported an error", e);

						} catch (AmazonClientException e) {
							logger.warn("Failed to delete message from queue after processing", e);
						}
					}
				}
				
			} catch (Throwable t) {
				logger.error(null, t);
				
			} finally {
				sleep(config.getLong(SLEEP_MILLIS_PROP_KEY));
			}
		}
	}

	private void sleep(Long millis) {
		try {
			Thread.sleep(millis);

		} catch (InterruptedException e) {
			logger.warn(null, e);
		}
	}
}
