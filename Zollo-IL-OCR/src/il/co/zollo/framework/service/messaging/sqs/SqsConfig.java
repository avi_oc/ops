package il.co.zollo.framework.service.messaging.sqs;

import il.co.zollo.framework.service.aws.AwsConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;

@Configuration
@Import({AwsConfig.class})
@PropertySource(value={"classpath:service.properties"})
public class SqsConfig {

	@Autowired
	private AWSCredentials credentials;
	
	@Bean
	public AmazonSQS sqsService(){
		AmazonSQSClient sqs = new AmazonSQSClient(credentials);
		sqs.withRegion(Regions.US_EAST_1);        
		return sqs;
	}
}
