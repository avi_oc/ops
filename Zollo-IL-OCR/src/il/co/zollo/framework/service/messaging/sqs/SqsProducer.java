package il.co.zollo.framework.service.messaging.sqs;

import il.co.zollo.framework.domain.messaging.Envelope;
import il.co.zollo.framework.service.messaging.MessageProducer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageRequest;

public class SqsProducer implements MessageProducer {

	public static final Logger logger = LoggerFactory.getLogger(SqsProducer.class);
	
	private AmazonSQS sqs;

	public SqsProducer(AmazonSQS sqs){
		this.sqs=sqs;
	}
	@Override
	public void produce(Envelope envelope) {
		logger.debug(new StringBuffer("Sending SQS message ").append(envelope.getMessage()).append(" to ")
				.append(envelope.getDestination()).toString());

		SendMessageRequest sendReq = new SendMessageRequest().withQueueUrl(envelope.getDestination())
				.withMessageBody(envelope.getMessage());
		sendReq.addMessageAttributesEntry(SENDER_ID_KEY,
				new MessageAttributeValue().withDataType("String").withStringValue(envelope.getSenderId()));
		sendReq.addMessageAttributesEntry(REF_ID_KEY,
				new MessageAttributeValue().withDataType("String").withStringValue(envelope.getRefId()));
		
		if (envelope.getContentType() != null) {
			sendReq.addMessageAttributesEntry(CONTENT_TYPE_KEY,
					new MessageAttributeValue().withDataType("String").withStringValue(envelope.getContentType()));			
		}
		if (envelope.getRequesterId() != null) {
			sendReq.addMessageAttributesEntry(REQUESTER_KEY,
					new MessageAttributeValue().withDataType("String").withStringValue(envelope.getRequesterId()));			
		}
		if (envelope.getPriority() != null) {
			sendReq.addMessageAttributesEntry(PRIORITY_KEY,
					new MessageAttributeValue().withDataType("Number").withStringValue(String.valueOf(envelope.getPriority())));			
		}		
		sqs.sendMessage(sendReq);
	}
}
