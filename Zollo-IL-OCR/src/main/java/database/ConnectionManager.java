package main.java.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author avi
 *
 */
public class ConnectionManager {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
//	static final String LOCAL_URL = "jdbc:mysql://localhost/ocr5?autoReconnect=true&useSSL=false";
//	static final String LOCAL_USER = "root";
//	static final String LOCAL_PASS = "5565183";
//	static final String P_RISDB_URL = "jdbc:mysql://ocrdsrr.caevgc8tzcpz.us-east-1.rds.amazonaws.com/risdb?autoReconnect=true&useSSL=false";	
//	static final String P_RISDB_USER = "ocuser";
//	static final String P_RISDB_PASS = "ocpass!1";
	static final String Q_ZIL_URL = "jdbc:mysql://test-rds.zollo.co.il/shopperztestdb?autoReconnect=true&useSSL=false";	
	static final String Q_ZIL_USER = "shopperztestuser";
	static final String Q_ZIL_PASS = "shopperztestpass";
//	static final String schemaQuery = "select {table}.* from risdb.{table} limit 1";
//	static final String unionQueryInto = "insert into {into} ";
//	static final String unionQueryFrom = "select {table}.* from {table}";
	//	static final String newLine=System.getProperty("line.separator");
	private Connection qadbconn = null;
//	private Connection risdbconn = null;
//	private Connection localdbconn = null;
	public final String timestamp;
//	private HashMap<String,String> results ;
	public ConnectionManager() {
		super();
		DateFormat dateFormat = new SimpleDateFormat("_yyyy_MM_dd_HH_mm_ss");
		Date date = new Date();
		this.timestamp=dateFormat.format(date);
//		this.results = new HashMap<String,String>();
	}
	public Connection getConnection(DB db){
		String url = null;
		String user = null;
		String pass = null;
		Connection conn = null;
		switch(db)
		{
		case Q_ZIL:
			conn=qadbconn;
			url = Q_ZIL_URL;
			user = Q_ZIL_USER;
			pass = Q_ZIL_PASS;
		}
		if(conn == null){
			try {
				Class.forName(JDBC_DRIVER);
				conn = DriverManager.getConnection(url, user, pass);
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		switch(db)
		{
		case Q_ZIL:
			qadbconn=conn;
			break;
		}
		return conn;
	}
	public void close(){
		try {
//			if(risdbconn != null) 	risdbconn.close();
//			if(localdbconn != null) localdbconn.close();
			if(qadbconn != null)	qadbconn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

//	public void copy(String query, DB from, DB to, String tableName,String retailerName) throws SQLException {
//		try (PreparedStatement s1 = getConnection(from).prepareStatement(query.replace("{table}", tableName));
//				ResultSet rs = s1.executeQuery())
//		{
//			copy(rs, from,to, tableName,retailerName);
//		}
//	}
//
	public void insertOCRRecords(List<String> incomingIds) throws SQLException{
		String qry = "INSERT IGNORE INTO ir_ocr SELECT * FROM incoming_receipt WHERE id IN ("+incomingIds.stream().collect(Collectors.joining(", "))+")";
		
		getConnection(DB.Q_ZIL).createStatement().executeUpdate(qry);
	}
	public void insertResultSet(ResultSet rs,DB db, String into) throws SQLException {
		ResultSetMetaData meta = rs.getMetaData();
		
		List<String> columns = new ArrayList<>();
		for (int i = 1; i <= meta.getColumnCount(); i++)
			columns.add(meta.getColumnName(i));

		try (PreparedStatement s2 = getConnection(db).prepareStatement(
				"INSERT INTO " + into + " (`"
						+ columns.stream().collect(Collectors.joining("`, `"))
						+ "`) VALUES ("
						+ columns.stream().map(c -> "?").collect(Collectors.joining(", "))
						+ ")"
				)) {

			while (rs.next()) {
				for (int i = 1; i <= meta.getColumnCount(); i++)
					s2.setObject(i, rs.getObject(i));
				s2.addBatch();
			}

			s2.executeBatch();
		}

	}
//	public HashMap<String, String> getResults() {
//		return results;
//	}
	private String getTimestamp() {
		return this.timestamp;
		
	}
//	public void createTableFromResultSet(ResultSet rs,DB db,String tableName) throws SQLException{
//
//		ResultSetMetaData rsmd = rs.getMetaData();
//		int columnCount = rsmd.getColumnCount();
//		StringBuilder sb = new StringBuilder( 1024 );
//		if ( columnCount > 0 ) { 
//			sb.append( "Create table " ).append( tableName ).append( " ( `" );
//		}
//		for ( int i = 1; i <= columnCount; i ++ ) {
//			if ( i > 1 ) sb.append( ", `" );
//			String columnName = rsmd.getColumnLabel( i );
//			String columnType = rsmd.getColumnTypeName( i );
//
//			sb.append( columnName ).append( "` " ).append( columnType );
//			if (!columnName.equals("created_on")){
//				int precision = rsmd.getPrecision( i );
//				if ( precision != 0 ) {
//					sb.append( "( " ).append( precision ).append( " )" );
//				}
//			}
//		} // for columns
//		sb.append( " ) " );
//		getConnection(db).createStatement().executeUpdate(sb.toString());
//
//	}
//	public List<String> getRefs(String selectSet, DB db) throws SQLException {
//		ResultSet rs = getConnection(db)
//				.prepareStatement(selectSet)
//				.executeQuery();
//		List<String> refs = new ArrayList<String>();
//		while (rs.next()) {
//			refs.add(rs.getString("ref_id"));
//		}
//		return refs;
//	}
//	public void deleteByRefs(String deleteQuery,DB db, String from) throws SQLException {
//		String query = deleteQuery.replace("{from}", from);
//		int i = getConnection(db).createStatement().executeUpdate(query);
//		System.out.println(i + " deleted from "+from);
//
//	}
//	public List<String> createScriptFromRefs(String query,DB db) throws SQLException {
//		List<String> lines = new ArrayList<String>();
//		ResultSet rs = getConnection(db).createStatement().executeQuery(query);
//		lines.add("#! /bin/bash");
//
//		while (rs.next()){
//			lines.add(rs.getString(1));
//		}
//		lines.add("exit;");
//		return lines;
//	}
	
}

