package main.java.ocr;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.sqs.AmazonSQSClient;

import main.java.database.ConnectionManager;

public abstract class Poller implements Runnable {
	
	protected String SQS_URL; //=null;//= "https://sqs.us-east-1.amazonaws.com/867154987980/qa_ocr_snapshot";
	protected long interval ;
	protected final String REF_ID_KEY = "ref_id";
	protected final String name = this.getClass().getSimpleName();
	protected ConnectionManager cm = null;
	protected AmazonSQSClient sqs;
	
	
	
	
	private void initSQS() {
		AWSCredentials credentials = null;
		try{
			credentials = new ProfileCredentialsProvider("OC").getCredentials();
		} catch (Exception e){
			throw new AmazonClientException(
		            "Cannot load the credentials from the credential profiles file. " +
		            "Please make sure that your credentials file is at the correct " +
		            "location (~/.aws/credentials), and is in valid format.",
		            e);
		}
		sqs = new AmazonSQSClient(credentials);
	}
	
	public Poller(long interval) {
		super();
		this.interval = interval;
		this.cm = new ConnectionManager();
		this.initSQS();
	}
	public Poller() {
		super();
		this.initSQS();
	}
	@Override
	public abstract void run();
}