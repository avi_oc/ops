package main.java.ocr;

import org.slf4j.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.sqs.AmazonSQSClient;

import il.co.zollo.framework.domain.messaging.Envelope;
import il.co.zollo.framework.domain.receipt.Receipt;
import il.co.zollo.framework.service.messaging.SenderBase;
import il.co.zollo.framework.service.messaging.sqs.SqsProducer;

public class SQSSender extends SenderBase{
	
	private AmazonSQSClient sqs;
	public SQSSender(){
		AWSCredentials credentials = null;
		try{
			credentials = new ProfileCredentialsProvider("OC").getCredentials();
		} catch (Exception e){
			throw new AmazonClientException(
		            "Cannot load the credentials from the credential profiles file. " +
		            "Please make sure that your credentials file is at the correct " +
		            "location (~/.aws/credentials), and is in valid format.",
		            e);
		}
		sqs = new AmazonSQSClient(credentials);
		this.producer= new SqsProducer(sqs);
	}
	public void sendObject(String contentType,Object obj,String refId,String sqsUrl) throws Exception{
		String message = mapper.writeValueAsString(obj);
		producer.produce(createEnvelope("GFK",sqsUrl, refId, 0, message, contentType, false));
		System.out.println(contentType+" sent with refId: "+refId);
	}
	@Override
	protected Logger getLogger() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void send(Envelope arg0) {
		// TODO Auto-generated method stub
		
	}
	private Envelope createEnvelope(String requesterId,String queue, String refId, Integer priority, String message, String contentType, Boolean useMturk) {
		// @formatter:off
		Envelope envelope = new Envelope().withDestination(queue)
			.withRefId(refId)
			.withRequesterId(requesterId)
			.withSenderId(requesterId)
			.withPriority(priority)
			.withContentType(contentType)
			.withUseMturk(useMturk)
			.withMessage(message);
		// @formatter:on
		return envelope;
	}
	
}
