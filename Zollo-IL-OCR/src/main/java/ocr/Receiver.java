package main.java.ocr;

import java.io.IOException;
import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ourcart.coding.ItemCoding;
import Entities.il.ilRawReceipt;
import il.co.zollo.framework.domain.raw.RawItem;
import il.co.zollo.framework.domain.raw.RawReceipt;
import il.co.zollo.framework.domain.receipt.Receipt;
import main.java.database.DB;

public class Receiver extends Poller {

	private static final String insertRows = "INSERT INTO `raw_receipt_row` (`col1`, `col2`, `col3`, `quality`,`raw_receipt_id`) VALUES (?,?,?,?,?)";
	private static final DB db = DB.Q_ZIL;

	public Receiver(long interval) {
		super(interval);
		this.SQS_URL="https://sqs.us-east-1.amazonaws.com/867154987980/qa_gfk_rawreceipt";
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run() {
		System.out.println(LocalTime.now()+" "+this.name+" started");
		while(true){			
			System.out.print(".");
			List<Message> messages = getMessagesFromQueue();
			for(Message m:messages){
				try {
					String refId=m.getMessageAttributes().get(REF_ID_KEY).getStringValue();
					System.out.println("Starting with refId: "+refId);
					RawReceipt rr = parseUSRawReceiptFromMessage(m);
					System.out.println("Raw receipt:");
					printObject(rr);
					Receipt r = interpretReceiptFromRawReceipt(rr,refId);
					if(Objects.nonNull(r)){
						System.out.println("Receipt:");
						printObject(r);
//						persistObject(r,r.getClass().getName(),refId);
//						persistObject(rr,rr.getClass().getName(),refId);
						deleteMessage(m);
					}else{
						System.out.println("Receipt result is null");
					}

				} catch (IOException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				break;
			}
		}
	}

	private void persistObject(Object obj,String contentType,String refId) throws Exception {
		SQSSender ss = new SQSSender();
		ss.sendObject(contentType, obj, refId
				,"https://sqs.us-east-1.amazonaws.com/867154987980/qa_persister_any");
	}
	private void printObject(Object obj) {
		System.out.println(new Gson().toJson(obj));
	}
	private List<Message> getMessagesFromQueue() {
		ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(SQS_URL)
				.withMessageAttributeNames(REF_ID_KEY);
		return sqs.receiveMessage(receiveMessageRequest).getMessages();	
	}

	private Receipt interpretReceiptFromRawReceipt(RawReceipt rr,String refId) throws IOException {
		Receipt receipt = null;
		try {
			System.out.println("Starting receipt inerpretation");
			receipt = interpretRawReceipt(rr,refId);
			System.out.println("Receipt inerpretation complete");
			return receipt;
		} catch (NullPointerException e) {
			System.out.println("Ovad made an exception :)");
			e.printStackTrace();
		}
		return null;
	}

	private void deleteMessage(Message message) {
		String messageReceiptHandle = message.getReceiptHandle();
		sqs.deleteMessage(new DeleteMessageRequest(SQS_URL, messageReceiptHandle));

	}


	private Receipt interpretRawReceipt(RawReceipt rr,String refId) {
		return ItemCoding.decode(rr,refId);
	}

	private Entities.il.ilRawReceipt ilRawReceiptFromMessage(Message message) throws IOException {
		RawReceipt usRawReceipt = parseUSRawReceiptFromMessage(message);
		Entities.il.ilRawReceipt ilRawReceipt = new Entities.il.ilRawReceipt();

		ilRawReceipt.setIncomingReceiptId(Integer.parseInt(message.getMessageAttributes().get(REF_ID_KEY).getStringValue()));
		ilRawReceipt.setTel(usRawReceipt.getRawStore()==null?null:usRawReceipt.getRawStore().getPhone());
		ilRawReceipt.setPurchaseTime(usRawReceipt.getRawStamp()==null?null:usRawReceipt.getRawStamp().getDate());
		ilRawReceipt.setVisualTotal(usRawReceipt.getRawTotals()==null?null:usRawReceipt.getRawTotals().getTotal());
		if(usRawReceipt.getRawItems()!=null){
			for (RawItem rawItem: usRawReceipt.getRawItems()) {
				try {
					Entities.il.ilRawReceipt.Row row = new Entities.il.ilRawReceipt.Row();
					String[] tokens = rawItem.getDesc().split("\\;", 3);
					row.setCol1(StringUtils.isNotBlank(tokens[0]) ? tokens[0] : "");
					row.setCol2(StringUtils.isNotBlank(tokens[1]) ? tokens[1] : "");
					row.setCol3(StringUtils.isNotBlank(tokens[2]) ? tokens[2] : "");
					row.setQuality(1.00);

					if (StringUtils.isNotBlank(row.getCol1()) || StringUtils.isNotBlank(row.getCol2()) || StringUtils.isNotBlank(row.getCol3())) {
						ilRawReceipt.addRow(row);
					}

				} catch (ArrayIndexOutOfBoundsException e) {
					e.printStackTrace();
				}
			}
		}
		return ilRawReceipt;
	}

	private RawReceipt parseUSRawReceiptFromMessage(Message message) throws IOException {
		return (RawReceipt)
				mapMessageToEntity(message.getBody(), RawReceipt.class);

	}
	//	private Message createMessageFromObject(Receipt receipt) throws IOException {
	//		return (Message)
	//				mapEntityToMessage(receipt);
	//
	//	}
	//	@SuppressWarnings({ "unchecked", "rawtypes" })
	//	protected Object mapEntityToMessage(Object obj) throws IOException {
	//		Message msg = new ObjectMapper().
	//				
	//		Object obj = new ObjectMapper().readValue(message, clazz);
	//		//			getLogger().debug("Envelope message mapped to object {}", obj.toString());
	//		return obj;
	//
	//	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected Object mapMessageToEntity(String message, Class clazz) throws IOException {

		Object obj = new ObjectMapper().readValue(message, clazz);
		//			getLogger().debug("Envelope message mapped to object {}", obj.toString());
		return obj;

	}
	private void persistIlRawReceipts(List<Message> messages) throws IOException{
		for(Message message:messages){
			Entities.il.ilRawReceipt rr = ilRawReceiptFromMessage(message);
			try {
				if(peristOneRawReceiptCompleted(rr)){
					deleteMessage(message);
					System.out.println(LocalTime.now()+" Received raw receipt from OCR with incoming ID " +rr.getIncomingReceiptId());
				}
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
	}
	private boolean peristOneRawReceiptCompleted(ilRawReceipt rr) throws SQLException {
		String qryInsertRawReceipt="INSERT INTO `raw_receipt` (`tel`, `purchase_time`, `visual_total`, `incoming_receipt_id`, `status`, `image_quality`, `calc_total`) VALUES"
				+"('"+rr.getTel()+"', '"+rr.getPurchaseTime()+"', '"+rr.getVisualTotal()+"', "+rr.getIncomingReceiptId()+", 'RE_PROCESS', 'OK', "+rr.getVisualTotal()+");";
		cm.getConnection(db).createStatement().executeUpdate(qryInsertRawReceipt);
		ResultSet rs = cm.getConnection(db).createStatement().executeQuery("SELECT LAST_INSERT_ID()");
		rs.next();
		rr.setId(((BigInteger)rs.getObject(1)).intValue());
		//TODO: getting the RR id using an additional query seems incorrect.  better to improve this...

		PreparedStatement stmt = cm.getConnection(db).prepareStatement(insertRows);

		for(Entities.il.ilRawReceipt.Row row : rr.getRows()){

			for(int i=1;i<4;i++){
				stmt.setString(i, row.getCol(i));
			}
			stmt.setDouble(4, row.getQuality());
			stmt.setInt(5, rr.getId());
			stmt.addBatch();
		}
		int[] inserted=stmt.executeBatch();
		if(inserted.length>0) 
			return true;
		else 
			return false;

	}
}
