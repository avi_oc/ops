package main.java.ocr;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;

import main.java.database.DB;

public class Sender extends Poller {
	private String qryGetIncomingReceipts = "select ir.* from incoming_receipt ir left join ir_ocr iro on iro.id=ir.id where ir.id>102996 and iro.id is null limit 5;";
	private String snapshotMessageBodyTemplate = "{\"url\":\"http://test-receipts.zollo.co.il/{URL}\",\"country\":\"IL\"}";
	private final String senderId = "ZIL";
	
	public Sender(long interval) {
		super(interval);
	}
	public Sender(String sqsUrl) {
		super();
		this.SQS_URL=sqsUrl;
	}
	protected ResultSet getIncomingReceipts(){
		try {
			return cm.getConnection(DB.Q_ZIL).createStatement().executeQuery(qryGetIncomingReceipts);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	private List<String> sendResultSetToSQSQueue(ResultSet rs) throws SQLException {
		List<String> list = new ArrayList<String>();
		while (rs.next()) {
			Integer id = (Integer)rs.getObject("id");
			System.out.println(LocalTime.now()+" Sending new incoming receipt to OCR. incoming ID: " + id);
			SendMessageResult smr = sqs.sendMessage(new SendMessageRequest()
				    .withQueueUrl(SQS_URL)
				    .withMessageBody(prepMessageBody(rs))
				    .withMessageAttributes(prepAttributes(rs)));
			if (smr.getSdkHttpMetadata().getHttpStatusCode()==200){
				list.add(id.toString());
				
			}else{
				System.out.println("HTTP status code different than 200: "+smr.getSdkHttpMetadata().getHttpStatusCode()+". Problematic incoming ID: "+id+". Skipping...");
			}
		}
		return list;
	}
//	private List<String> sendMessageToSQSQueue(Message message) throws SQLException {
//		List<String> list = new ArrayList<String>();
//		
//			String refId = message.getAttributes().get("ref_id");
//			System.out.println(LocalTime.now()+" Sending new receipt to result queue. refID: " + refId);
//			SendMessageResult smr = sqs.sendMessage(new SendMessageRequest()
//				    .withQueueUrl(SQS_URL)
//				    .withMessageBody(prepMessageBody(rs))
//				    .withMessageAttributes(prepAttributes(rs)));
//			if (smr.getSdkHttpMetadata().getHttpStatusCode()==200){
//				list.add(id.toString());
//				
//			}else{
//				System.out.println("HTTP status code different than 200: "+smr.getSdkHttpMetadata().getHttpStatusCode()+". Problematic incoming ID: "+id+". Skipping...");
//			}
//		
//		return list;
//	}
	private Map<String, MessageAttributeValue> prepAttributes(ResultSet rs) throws SQLException {
		Map<String, MessageAttributeValue> messageAttributes = new HashMap<>();
		messageAttributes.put("sender_id", new MessageAttributeValue().withDataType("String").withStringValue(senderId));
		messageAttributes.put("requester_id", new MessageAttributeValue().withDataType("String").withStringValue(senderId));
		Integer refid = (Integer)rs.getObject("id");
		messageAttributes.put(REF_ID_KEY, new MessageAttributeValue().withDataType("String").withStringValue(refid.toString()));
		messageAttributes.put("content_type", new MessageAttributeValue().withDataType("String").withStringValue("Snapshot"));
		return messageAttributes;
	}
	
	private String prepMessageBody(ResultSet rs) throws SQLException {
		return  snapshotMessageBodyTemplate.replace("{URL}", (String)rs.getObject("image_location"));
		
	}
	@Override
	public void run() {
		System.out.println(LocalTime.now()+" "+ this.name+" started");
		while (true){
			ResultSet rs = getIncomingReceipts();
			List<String> sentToOCR=null;
			
			try {
				sentToOCR = sendResultSetToSQSQueue(rs);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			if(sentToOCR.size()>0){
				try {
					cm.insertOCRRecords(sentToOCR);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				break;
			}
		}
		
	}
	public void sendMessages(List<Message> messages) {
		for(Message message:messages){
			
		}
		
	}
	
	

}
