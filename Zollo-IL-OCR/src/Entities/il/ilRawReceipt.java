package Entities.il;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.lang.StringUtils;


public class ilRawReceipt {

	public enum ReceiptImageQuality {
		NOT_RECEIPT, BLURRY, SIDE_CUT, NO_TEL, OK;
	};
	private Integer id;
	private String tel;
	private String purchaseTime;
	private String sourceId;
	private Integer incomingReceiptId;
	private ReceiptImageQuality receiptImageQuality;
	private String visualTotal;
	private List<Row> rows = new ArrayList<Row>();
	private boolean reprocess = false;
	

	public int size() {
		int size = 0;
		
		if (rows != null) {
			size = rows.size();
		}
		return size;
	}
	
	public String getTel() {
		return StringUtils.trim(StringUtils.remove(tel,'-'));
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public void setPurchaseTime(String purchaseTime) {
		this.purchaseTime = purchaseTime;
	}

	public String getPurchaseTime() {
		return StringUtils.trim(purchaseTime);
	}
	public Integer getIncomingReceiptId() {
		return incomingReceiptId;
	}

	public void setIncomingReceiptId(Integer incomingReceiptId) {
		this.incomingReceiptId = incomingReceiptId;
	}

	public List<Row> getRows() {
		return rows;
	}

	public void setRows(List<Row> rows) {
		this.rows = rows;
	}

	public void addRow(Row row) {
		rows.add(row);
	}

	public ListIterator<Row> rowIterator() {
		if (rows != null) {
			return rows.listIterator();
		}
		return null;
	}

	public String getCompleteTel() {
		if (StringUtils.isNotEmpty(tel)) {
			return tel;
		}
		return tel;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public ReceiptImageQuality getReceiptImageQuality() {
		return receiptImageQuality;
	}

	public void setReceiptImageQuality(ReceiptImageQuality receiptImageQuality) {
		this.receiptImageQuality = receiptImageQuality;
	}

	public void setReceiptImageQuality(String receiptImageQualityStr) {
		this.receiptImageQuality = ReceiptImageQuality.valueOf(StringUtils.upperCase(receiptImageQualityStr));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RawReceipt [id=");
		builder.append(id);
		builder.append(", tel=");
		builder.append(tel);
		builder.append(", purchaseTime=");
		builder.append(purchaseTime);
		builder.append(", sourceId=");
		builder.append(sourceId);
		builder.append(", incomingReceiptId=");
		builder.append(incomingReceiptId);
		builder.append(", receiptImageQuality=");
		builder.append(receiptImageQuality);
		builder.append(", rows=");
		builder.append(rows);
		builder.append("]");
		return builder.toString();
	}

	public static class Row {
		private Integer id;
		private String col1 = null;
		private String col2 = null;
		private String col3 = null;
		private boolean isSkipped = false;
		private Double quality;
		private Integer rawReceiptId;

		public boolean getIsSkipped() {
			return isSkipped;
		}
		public void setIsSkipped(boolean isSkipped) {
			this.isSkipped = isSkipped;
		}
		public String getCol1() {
			return StringUtils.trim(col1);
		}
		public void setCol1(String col1) {
			this.col1 = col1;
		}
		public String getCol2() {
			return StringUtils.trim(col2);
		}
		public void setCol2(String col2) {
			this.col2 = col2;
		}
		public String getCol3() {
			return StringUtils.trim(col3);
		}
		public void setCol3(String col3) {
			this.col3 = col3;
		}
		public String getCol(int col) {
			switch (col) {
				case 1 :
					return getCol1();
				case 2 :
					return getCol2();
				case 3 :
					return getCol3();
			}
			return "";
		}
		public Double getQuality() {
			return quality;
		}
		public void setQuality(Double quality) {
			this.quality = quality;
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public Integer getRawReceiptId() {
			return rawReceiptId;
		}
		public void setRawReceiptId(Integer rawReceiptId) {
			this.rawReceiptId = rawReceiptId;
		}
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("Row [id=");
			builder.append(id);
			builder.append(", col1=");
			builder.append(col1);
			builder.append(", col2=");
			builder.append(col2);
			builder.append(", col3=");
			builder.append(col3);
			builder.append(", quality=");
			builder.append(quality);
			builder.append(", rawReceiptId=");
			builder.append(rawReceiptId);
			builder.append("]");
			return builder.toString();
		}
	}

	public String getVisualTotal() {
		return visualTotal;
	}

	public void setVisualTotal(String visualTotal) {
		this.visualTotal = visualTotal;
	}

	public boolean isReprocess() {
		return reprocess;
	}

	public void setReprocess(boolean reprocess) {
		this.reprocess = reprocess;
	}


}
